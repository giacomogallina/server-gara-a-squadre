#[macro_use]
extern crate serde_derive;
extern crate toml;
extern crate rumqtt;
#[macro_use]
extern crate serde_json;
extern crate mqtt;
use std::time::{Duration, SystemTime};
use std::io;
use std::io::Read;
use std::fs::{File, OpenOptions};
use std::io::prelude::*;
use std::sync::mpsc::{Receiver, Sender, channel};
use std::sync::{Mutex, Arc};
use std::thread;
use std::cmp::min;
use rumqtt::{MqttOptions, MqttClient, MqttCallback, QoS};
use std::str;
use std::path::Path;
use mqtt::topic_name::TopicName;

#[derive(Serialize, Deserialize, Debug)]
struct Answer {
    team: usize,
    problem: usize,
    value: i32,
}

#[derive(Serialize, Deserialize, Debug)]
struct AnswerRecord {
    answer: Answer,
    //time: SystemTime,
    time: f64,
    enabled: bool,
}

impl AnswerRecord {
    fn new(ans: Answer, t: f64) -> AnswerRecord {
        AnswerRecord {
            answer: ans,
            //time: SystemTime::now(),
            time: t,
            enabled: true,
        }
    }
}

enum InternalMessage {
    Start,
    Info,
    Resume,
    Ans(Answer),
}


#[derive(Deserialize, Clone)]
struct BrokerConfig {
    address: String,
    user: String,
    password: String,
}

#[derive(Deserialize)]
struct ContestConfig {
    teams: Vec<String>,
    correct_answers: Vec<i32>,
    duration: i32,
    deriva: Option<i32>,
}

#[derive(Deserialize)]
struct Config {
    broker: BrokerConfig,
    contest: ContestConfig,
}

#[derive(Clone)]
struct ProblemStatus {
    points: i32,
    solved: bool,
}

impl ProblemStatus {
    fn new() -> ProblemStatus {
        ProblemStatus {
            points: 0,
            solved: false,
        }
    }
}

struct Contest {
    teams: Vec<String>,
    answers: Vec<AnswerRecord>,
    correct_answers: Vec<i32>,
    resume_time: Option<SystemTime>,
    previously_passed_time: f64,
    duration: f64,
    broker: BrokerConfig,
    mq_client: Option<MqttClient>,
    channel_rx: Receiver<InternalMessage>,
    channel_tx: Arc<Mutex<Sender<InternalMessage>>>,
    bonuses: Vec<i32>,
    deriva: i32,
    save_file: String,
}

impl Contest {
    fn new() -> Contest {
        let (tx, rx) = channel();
        Contest {
            teams: vec![],
            answers: vec![],
            correct_answers: vec![],
            resume_time: None,
            previously_passed_time: 0.0,
            duration: 120.0*60.0,
            broker: BrokerConfig {
                address: String::new(),
                user: String::new(),
                password: String::new(),
            },
            mq_client: None,
            channel_rx: rx,
            channel_tx: Arc::new(Mutex::new(tx)),
            bonuses: vec![20, 15, 10, 8, 6, 5, 4, 3, 2, 1],
            deriva: 2,
            save_file: "save.txt".to_string(),
        }
    }

    fn import_config(&mut self) {
        let mut config_file = File::open("config.toml").expect("cannot open config file");
        let mut contents = String::new();
        config_file.read_to_string(&mut contents).expect("cannot correctly read config file");
        
        let config: Config = toml::from_str(&contents).expect("cannot decode config file");

        //println!("{}", config.teams[0]);

        self.correct_answers = config.contest.correct_answers.clone();
        self.teams = config.contest.teams;
        self.duration = config.contest.duration as f64 *60.0;
        self.broker = config.broker;
        if let Some(d) = config.contest.deriva {
            self.deriva = d;
        }
    }

    fn connect(&mut self) {
        let options = MqttOptions::new()
                            .set_keep_alive(5)
                            .set_reconnect(3)
                            .set_client_id("server")
                            .set_broker(&self.broker.address)
                            .set_user_name(&self.broker.user)
                            .set_password(&self.broker.password);
        let mq_tx = Arc::clone(&self.channel_tx);
        let callback = MqttCallback::new().on_message(move |message| {
            println!("message --> {:?}", message);
            let mq_tx = mq_tx.lock().unwrap();
            match serde_json::from_slice(&*message.payload) {
                Ok(ans) => mq_tx.send(InternalMessage::Ans(ans)).unwrap(),
                Err(_) => (),
            }
            if message.topic == TopicName::new("ask_info".to_string()).unwrap() {
                mq_tx.send(InternalMessage::Info).unwrap();
            }
        });
        let mut client = MqttClient::start(options, Some(callback)).expect("cannot connect to mqtt broker");
        let topics = vec![("answers", QoS::Level2), ("ask_info", QoS::Level2)];
        client.subscribe(topics).expect("cannot subscribe to topics");
        self.mq_client = Some(client);
    }

    fn start(&mut self) {
        let mut f = OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(&self.save_file)
            .expect("cannot create new blank save file");

        match f.write(b"0.0\n") {
            Ok(_) => (),
            Err(e) => println!("cannot write to save file: {}", e),
        };

        self.resume_time = Some(SystemTime::now());
        for _m in self.channel_rx.try_iter() { };
    }

    fn contest_time(&self) -> Option<f64> {
        match self.resume_time {
            Some(rt) => {
                let time_since_resume = rt.elapsed().unwrap();
                Some(time_since_resume.as_secs() as f64 +
                    time_since_resume.subsec_nanos() as f64 / 1000000000.0 +
                    self.previously_passed_time)
            },
            None => None,
        }
    }

    fn resume(&mut self) {
        let mut f = File::open(&self.save_file).expect("cannot open save file");
        let mut contents = String::new();
        f.read_to_string(&mut contents).expect("cannot correctly read save file");
        for (l, n) in contents.lines().zip(1..) {
            match l.parse::<f64>() {
                Ok(t) => self.previously_passed_time = t,
                Err(_) => {
                    match serde_json::from_str(l) {
                        Ok(ans) => self.answers.push(ans),
                        Err(_) => println!("line {} of {} cannot be decoded", n, self.save_file),
                    }
                }
            }
        }
        self.resume_time = Some(SystemTime::now());
        for _m in self.channel_rx.try_iter() { };
    }

    fn save(&self, s: &str) {
        //let mut f = File::open(&self.save_file).expect("cannot open save file");
        match OpenOptions::new()
            .write(true)
            .create(true)
            .append(true)
            .open(&self.save_file) {

            Ok(mut f) => {
                match f.write(s.as_bytes()) {
                    Ok(_) => (),
                    Err(e) => println!("cannot write to save file: {}", e),
                };
            },
            Err(e) => println!("cannot open save file: {}", e),
        }
    }


    fn run(&mut self) {
        let mut should_start;
        let mut should_resume;

        for count in 0.. {
            thread::sleep(Duration::from_secs(1));
            should_start = false;
            should_resume = false;
            for im in self.channel_rx.try_iter() {
                match im {
                    InternalMessage::Start => {
                        match self.resume_time {
                            Some(_) => (),
                            None => { should_start = true; break; },
                        }
                    },
                    InternalMessage::Ans(a) => {
                        if a.team < self.teams.len() && a.problem < self.correct_answers.len() {
                            if let Some(t) = self.contest_time() {
                                let ansr = AnswerRecord::new(a, t);
                                self.save(&format!("{}\n", json!(ansr).to_string()));
                                self.answers.push(ansr);
                            }
                        }
                    },
                    InternalMessage::Info => {
                        if let Some(ref mut client) = self.mq_client {
                            let info = json!({
                                "teams": self.teams,
                                "problems": self.correct_answers.len(),
                            }).to_string();
                            client.publish("info", QoS::Level2, info.into_bytes()).expect("cannot publish info");
                        }
                    },
                    InternalMessage::Resume => {
                        match self.resume_time {
                            Some(_) => (),
                            None => { should_resume = true; break; },
                        }
                    },
                }
            }

            if should_resume {
                self.resume();
            }

            if should_start {
                self.start();
            }

            let mut jollys: Vec<Option<usize>> = vec![None; self.teams.len()];

            let mut problem_values = vec![20; self.correct_answers.len()];
            let mut solved = vec![0; self.correct_answers.len()];
            let mut status = vec![vec![ProblemStatus::new(); self.correct_answers.len()]; self.teams.len()];

            if let Some(t) = self.contest_time() {
                if count % 10 == 0 {
                    self.save(&format!("{}\n", t));
                }

                for a in &self.answers {
                    if a.answer.value == -1 {
                        if a.time < 600.0 {
                            if jollys[a.answer.team] == None {
                                jollys[a.answer.team] = Some(a.answer.problem);
                            }
                        }
                    }
                    else {
                        if a.answer.value == self.correct_answers[a.answer.problem] {
                            if !status[a.answer.team][a.answer.problem].solved {
                                status[a.answer.team][a.answer.problem].solved = true;
                                status[a.answer.team][a.answer.problem].points += self.bonuses[min(solved[a.answer.problem] as usize, self.bonuses.len()-1)];
                                solved[a.answer.problem] += 1;
                                if solved[a.answer.problem] == self.deriva {
                                    problem_values[a.answer.problem] += min((a.time / 60.0) as i32, (self.duration/60.0) as i32 - 20);
                                }
                            }
                        }
                        else {
                            status[a.answer.team][a.answer.problem].points -= 10;
                            if a.time < self.duration - 20.0*60.0 {
                                problem_values[a.answer.problem] += 2;
                            }
                        }
                    }
                }

                for (n, p) in solved.iter().zip(0..) {
                    if *n < self.deriva {
                        problem_values[p] += min(t as u64 /60, self.duration as u64 /60 - 20) as i32;
                    }
                }

                for t in 0..self.teams.len() {
                    for p in 0..self.correct_answers.len() {
                        if status[t][p].solved {
                            status[t][p].points += problem_values[p];
                        }
                    }
                }

                if t > 600.0 {
                    for j in &mut jollys {
                        if *j == None {
                            *j = Some(0);
                        }
                    }
                }

                for (j, t) in jollys.iter().zip(0..) {
                    if let Some(n) = *j {
                        status[t][n].points *= 2;
                    }
                }

                print!("\nTime: {}", t);
                for t in 0..self.teams.len() {
                    print!("\n{}:", self.teams[t]);
                    let mut total_points = 10*self.correct_answers.len() as i32;
                    for p in 0..self.correct_answers.len() {
                        total_points += status[t][p].points;
                    }
                    print!(" {} |", total_points);
                    for p in 0..self.correct_answers.len() {
                        print!("{} ", status[t][p].points);
                    }
                }
                println!("");
            }

            let time_left = match self.contest_time() {
                Some(st) => Some((self.duration - st) as u64),
                None => None,
            };

            match self.mq_client {
                None => panic!("missing mqtt client!"),
                Some(ref mut client) => {
                    let contest_status = json!({
                        "time_left": time_left,
                        "teams": self.teams,
                        "problem_values": problem_values,
                        "points": status.iter().map(|v| {
                            v.iter().map(|c| {
                                c.points
                            }).collect::<Vec<i32>>()
                        }).collect::<Vec<Vec<i32>>>(),
                        "solved": status.iter().map(|v| {
                            v.iter().map(|c| {
                                match c.solved {
                                    true => 1,
                                    false => 0,
                                }
                            }).collect::<Vec<i32>>()
                        }).collect::<Vec<Vec<i32>>>(),
                        "jollys": jollys,
                    }).to_string();
                    //println!("{}", contest_status);
                    match client.publish("status", QoS::Level2, contest_status.into_bytes()) {
                        Ok(_) => (),
                        Err(_) => println!("cannot publish status"),
                    };
                },
            }

            match self.contest_time() {
                Some(t) => {
                    if t > self.duration {
                        break;
                    }
                },
                None => (),
            }
            //println!("{:?}", self.answers);
            //println!("{:?}", problem_values);
        }
    }
}




fn main() {

    let mut contest = Contest::new();
    contest.import_config();
    contest.connect();

    let tx = Arc::clone(&contest.channel_tx);

    thread::spawn(move || {
        let mut msg = InternalMessage::Start;
        let mut action = "start";
        let mut reply = String::new();
        if Path::new("save.txt").exists() {
            println!("A save file already exists: do you want to resume the previous competition? [Y/n]");
            io::stdin().read_line(&mut reply).expect("cannot read reply");
            if reply.trim() == "y" || reply.trim() == "Y" {
                msg = InternalMessage::Resume;
                action = "resume";
            }
        }
        println!("\nPress Enter to {} the competition", action);
        io::stdin().read_line(&mut reply).expect("cannot read reply");

        println!("\nStarting competition!\n");
        let mq_tx = tx.lock().unwrap();
        mq_tx.send(msg).unwrap();
    });
        
    contest.run();



    //println!("Hello, world!");
    //thread::sleep(Duration::from_secs(600));
}
